# Recipe App API Proxy
NGINX Proxy app for out recipe app API

## Usage 
### Enviromnet Variables 
* `LISTEN_PORT` - Port to Listen on (default: `8000`)
* `App_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)